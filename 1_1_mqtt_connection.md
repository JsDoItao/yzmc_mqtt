
# MQTT 接入

## 角色

- 设备
- mqtt-broker
- 处理模块: 数据处理模块,下发命令模块

![mqtt接入](/assets/xizi_mqtt.png)

## 长连接

- 设备与broker之间,下发命令模块与broker之间,实时数据处理模块和broker之间,均为单一长连接
- 要求broker能够支持10k+同时在线/并发服务设备

## 身份认证

- 用户名: 设备id, 密码: md5(设备id+yzmc)
- 必须带, 设备id必须存在库中

## topic订阅/发布

### 设备
- 设备订阅topic: "设备ID", 用来接收服务器下发命令
- 设备发布topic: "xizi_report", 来实时上报数据,1次/秒

### 平台处理
- 下发命令模块发布topic: "设备ID", 对在线设备进行执行命令处理
- 实时数据处理模块订阅topic: "xizi_report", 进行数据处理

## 消息体格式

- xizi_report格式,按照先前厂商定义格式
- 下发命令格式:
    - 升级: upgrade,版本,http-url,md5值
    - 重启: restart,ip:port
    
## 设备连接流程(为了便于broker服务器横向扩展,需要先获取可用服务器列表)

1. 首先http请求, http://io.yizhimc.com/server (草稿,待部署), 将返回一个或者多个ip1:port2,ip2:port2,... 以逗号分隔的连接ip和端口信息,设备将随机选取其一来进行长连接
2. 选取ip:port之后,进行mqtt长连接

## 设备探活

- 可以尝试用mqtt的PINGREQ/PINGRESP来处理;
- 注意mqtt client连接的若干参数设置,比如重连次数, keepalive的PING保活间隔;

## Mosquito的优化——epoll优化

- [https://blog.csdn.net/houjixin/article/details/46413583]