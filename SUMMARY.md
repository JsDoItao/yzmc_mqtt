# Summary

* [设计目标](0_design_target.md)
* [接入层设计](1_0_connection_design.md)
    * [mqtt接入](1_1_mqtt_connection.md)
    * [http/websocket接入](1_2_http_websocket_connection.md)
* [data设计](2_0_data_store.md)



